A port of Hugo's Hemingway theme by tanksuzuki to Nikola.

Original theme: https://themes.gohugo.io/hemingway/

And Original Port is made by Roberto Alsina.

https://themes.getnikola.com/v8/hemingway/

I applied following improvements:

- applied lates Bulma CSS Framework
- migrated to Feather icon from Font Awesome

License is MIT

The navigation menu has no text, only ``(link, class)`` where class
is a Font Awesome class, like ``"fa fa-github"``
