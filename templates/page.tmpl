
## -*- coding: utf-8 -*-
<%namespace name="helper" file="post_helper.tmpl"/>
<%namespace name="pheader" file="post_header.tmpl"/>
<%namespace name="comments" file="comments_helper.tmpl"/>
<%namespace name="math" file="math_helper.tmpl"/>
<%inherit file="base.tmpl"/>

<%block name="extra_head">
    ${parent.extra_head()}
    % if post.meta('keywords'):
    <meta name="keywords" content="${post.meta('keywords')|h}">
    % endif
    <meta name="author" content="${post.author()|h}">
    %if post.prev_post:
        <link rel="prev" href="${post.prev_post.permalink()}" title="${post.prev_post.title()|h}" type="text/html">
    %endif
    %if post.next_post:
        <link rel="next" href="${post.next_post.permalink()}" title="${post.next_post.title()|h}" type="text/html">
    %endif
    % if post.is_draft:
        <meta name="robots" content="noindex">
    % endif
    ${helper.open_graph_metadata(post)}
    ${helper.twitter_card_information(post)}
    ${helper.meta_translations(post)}
    ${math.math_styles_ifpost(post)}
</%block>

<%block name="content">
    <article class="section">
        <h2 class="subtitle is-6"><time datetime="${post.formatted_date('webiso')}"
        itemprop="datePublished" title="${post.formatted_date(date_format)|h}">
        ${post.formatted_date(date_format)|h}
        </time></h2>
        <h1 class="title is-3 is-size-4-touch">${post.title()|h}</h1>
        % if post.updated and post.updated != post.date:
            <p class="updated"> (${messages("updated")}
                <time class="dt-updated" datetime="${post.formatted_updated('webiso')}" itemprop="dateUpdated" title="${post.formatted_updated(date_format)|h}">${post.formatted_updated(date_format)|h}</time>)</p>
        % endif
        <div class="content">
            ${post.text()}
        </div> <!-- content -->
        <nav class="container">
            ${helper.html_pager(post)}
        <nav>
    </article>
    <section class="section">
            % if not post.meta('nocomments') and site_has_comments:
            <dev class="container comments hidden-print">
            <h2>${messages("Comments")}</h2>
            ${comments.comment_form(post.permalink(absolute=True), post.title(), post._base_path)}
            </div>
        % endif
    </section>
${math.math_scripts_ifpost(post)}
${comments.comment_link_script()}
</%block>
